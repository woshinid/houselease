package com.kgc.cn.config;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * Created by boot on 2019/12/5.
 */
@Configuration
@MapperScan("com.kgc.cn.kh73_service.mapper")
public class MybatisConfig {
    /*@Autowired
    @Qualifier("dynamicDataSource")*/
    @Resource(name = "dynamicDataSource")
    private DataSource dataSource;


    @Bean
    public SqlSessionFactoryBean buildSqlSessionFactory(){
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource);
        return sqlSessionFactoryBean;
    }


}
