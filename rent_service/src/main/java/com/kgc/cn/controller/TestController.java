package com.kgc.cn.controller;

import com.kgc.cn.model.User;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/test")
public class TestController {

    @PostMapping(value = "/first")
    public User firstTest(@RequestBody User user){
        return user;
    }
}
