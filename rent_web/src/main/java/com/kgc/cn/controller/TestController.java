package com.kgc.cn.controller;

import com.google.common.collect.Lists;
import com.kgc.cn.TestService;
import com.kgc.cn.User;
import com.kgc.cn.config.file.FileUpdateProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Api(tags = "test")
@RestController
@RequestMapping(value = "/test")
public class TestController {

    @Autowired
    private TestService testService;

    @Autowired
    private FileUpdateProperties fileUpdateProperties;

    @ApiOperation(value = "传对象")
    @PostMapping(value = "/first")
    public User firstTest(@Valid User user){
        return testService.firstTest(user);
    }


    @PostMapping(value = "insertDetail")
    public int addDetail(List<MultipartFile> multipartFileImg,HttpServletRequest request) {
        String path = fileUpdateProperties.getUploadFolder()+"\\";
        String name = request.getParameter("name");
        List<String> urls = Lists.newArrayList();

        String imgUrlStr = "";
        if (multipartFileImg != null) {
            for (int i = 0; i < multipartFileImg.size(); i++) {
                //获取文件原始名
                String fileName = multipartFileImg.get(i).getOriginalFilename();
                //获取文件后缀名
                String fileType = fileName.substring(fileName.lastIndexOf("."));
                try {
                    //获取输入流
                    InputStream ins = multipartFileImg.get(i).getInputStream();
                    //文件保存目录
                    String filePath = path + "/" + name;
                    File fi = new File(filePath);
                    //如果文件夹不存在则创建
                    if (!fi.exists()) {
                        fi.mkdirs();
                    }
                    FileOutputStream out = null;
                    if (i < 3) {
                        out = new FileOutputStream(filePath + "/thumbnails" + (i + 1) + fileType);
                        urls.add("http://localhost:9001/upload/" + name + "/thumbnails" + (i + 1) + fileType);
                    } else {
                        out = new FileOutputStream(filePath + "/detail" + (i - 2) + fileType);
                        urls.add("http://localhost:9001/upload/" + name + "/detail" + (i - 2) + fileType);
                    }
                    int read = 0;
                    byte[] buffer = new byte[4096];
                    while ((read = ins.read(buffer)) != -1) {
                        out.write(buffer, 0, read); // 保存文件数据
                    }
                    out.flush();
                    out.close();
                    ins.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        //newAdd.setPictures(url);
        //将问题图片url集合存入数据库
        //rulerService.addDetail(newAdd);
        /*}*/
        return 1;
    }
}
