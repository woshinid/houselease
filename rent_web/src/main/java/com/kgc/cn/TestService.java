package com.kgc.cn;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "kh73-service", fallback = TestService.testImpl.class)
public interface TestService {

    @GetMapping(value = "/test/first")
    User firstTest(@RequestBody User user);

    @Component
    class testImpl implements TestService{
        @Override
        public User firstTest(User user) {
            return null;
        }
    }
}
